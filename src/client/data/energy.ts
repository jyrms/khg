import { SimpleData } from '../types'
import { parseSimpleData } from './simple-data'

let energyData: SimpleData

export const loadEnergyData = async (): Promise<SimpleData> => {
  if (energyData) {
    return energyData
  }

  const res = await fetch('data/energy.ghgs')
  const strData = await res.text()
  energyData = parseSimpleData(strData)

  return energyData
}