import { fieldSeparator, lineSeparator, parseAmount, parseToken, parseYears } from '../data'
import { Row, SimpleData } from '../types'

enum DataToken {
  id = 'id',
  name = 'name',
  years = 'years',
  rows = 'rows',
  text = 'text',
}

const parseRows = (lines: string[]): Row[] => {
  const rows: Row[] = []

  while (true) {
    const line = lines.shift()
    if (!line || !line.trim()) break

    const [name, ...strVals] = line.split(fieldSeparator)
    const data = strVals.map(parseAmount)
    rows.push({ name, data })
  }

  return rows
}

export const parseSimpleData = (strData: string): SimpleData => {
  const lines = strData.split(lineSeparator)
  const sd: Partial<SimpleData> = {}

  while (lines.length) {
    const line = lines.shift().trim()

    if (!line) continue

    const { token, rest } = parseToken(line)

    switch (token) {
      case DataToken.id:
        sd.id = rest
        break
      case DataToken.name:
        sd.name = rest
        break
      case DataToken.years:
        const { startYear, numYears } = parseYears(rest)
        sd.startYear = startYear
        sd.numYears = numYears
        break
      case DataToken.rows:
        sd.rows = parseRows(lines)
        break
      case DataToken.text:
        sd.text = rest
        break
      default:
        throw new Error('Invalid simple data token: ' + token)
    }
  }

  return sd as SimpleData
}