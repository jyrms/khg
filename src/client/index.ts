import { createElement as el } from 'react'
import * as ReactDOM from 'react-dom'

import { Main } from './main'

window.onload = () => {
  const domRoot = document.getElementById('root')
  ReactDOM.render(el(Main), domRoot)
}
