import { assertNever } from './assert-never'
import { cats, sectors, subCats } from './segments'

import {
  Sector,
  Cat,
  SubCat,
  GasId,
  GasesData,
  Gas,
  YearlyData,
  DetailLevel,
  Bunkers,
  GasTotals,
  Proportion,
  Segment,
  ProportionData,
  PeriodType,
  Measure,
} from './types'

enum DataToken {
  id = 'id',
  name = 'name',
  unit = 'unit',
  years = 'years',
  sectors = 'sectors',
  memo = 'memo',
  total = 'total',
}

enum MemoToken {
  bunkers = 'bunkers',
  aviation = 'aviation',
  navigation = 'navigation',
}

enum TotalsToken {
  textWithout = 'textWithout',
  textWith = 'textWith',
  without = 'without',
  with = 'with',
}

interface TokenizedLine<T> {
  token: T,
  rest?: string,
}

interface Years {
  startYear: number,
  numYears: number,
}

interface SegmentSpec {
  detailLevel: DetailLevel,
  id: string,
  yearlyData: YearlyData,
}

export const lineSeparator = '\n'
const tokenSeparator = ': '
const yearSeparator = '-'
export const fieldSeparator = '\t'

const isDataToken = (strToken: string): strToken is DataToken => strToken in DataToken

export const parseToken = (line: string): TokenizedLine<string> => {
  const [token, rawRest] = line.split(tokenSeparator)
  const tokenizedLine: TokenizedLine<string> = { token }

  if (rawRest !== undefined) {
    tokenizedLine.rest = rawRest.trim()
  }

  return tokenizedLine
}

const parseDataToken = (line: string): TokenizedLine<DataToken> => {
  const tokenizedLine = parseToken(line)

  if (!isDataToken(tokenizedLine.token)) {
    throw new Error('Unrecognized data token: ' + tokenizedLine.token)
  }

  return tokenizedLine as TokenizedLine<DataToken>
}

const isGasId = (id: string): id is GasId => id in GasId

export const parseYears = (yearsToken: string): Years => {
  const [strStartYear, strEndYear] = yearsToken.split(yearSeparator)
  const startYear = parseInt(strStartYear, 10)
  const endYear = parseInt(strEndYear, 10)
  const numYears = (endYear - startYear) + 1
  return { startYear, numYears }
}

const validateSegmentId = (id: string, detailLevel: DetailLevel) => {
  if (detailLevel === DetailLevel.sector) {
    if (!sectors.has(id)) {
      throw new Error('Invalid sector id: ' + id)
    }
  } else if (detailLevel === DetailLevel.cat) {
    if (!cats.has(id)) {
      throw new Error('Invalid category id: ' + id)
    }
  } else if (detailLevel === DetailLevel.subCat) {
    if (!subCats.has(id)) {
      throw new Error('Invalid sub-category id: ' + id)
    }
  } else {
    throw new Error('Invalid detail level: ' + detailLevel)
  }
}

export const parseAmount = (str: string) => {
  const floatAmt = parseFloat(str)
  return isNaN(floatAmt) ? 0 : floatAmt
}

const parseSegmentLine = (line: string): SegmentSpec => {
  let detailLevel: DetailLevel

  if (line.startsWith(fieldSeparator + fieldSeparator)) {
    detailLevel = DetailLevel.subCat
  } else if (line.startsWith(fieldSeparator)) {
    detailLevel = DetailLevel.cat
  } else {
    detailLevel = DetailLevel.sector
  }

  const trimmedLine = line.trim()
  const [id, ...strYearlyData] = trimmedLine.split(fieldSeparator)
  validateSegmentId(id, detailLevel)
  const yearlyData = strYearlyData.map(parseAmount)
  return { detailLevel, id, yearlyData }
}

const parseSectors = (lines: string[]): Sector[] => {
  let sectors: Sector[] = []
  let detailLevel: DetailLevel = DetailLevel.sector
  let sector: Sector
  let cat: Cat
  let subCat: SubCat
  let line: string

  while (true) {
    line = lines.shift()

    if (!line || !line.trim()) break

    const segmentSpec = parseSegmentLine(line)
    detailLevel = segmentSpec.detailLevel

    if (detailLevel === DetailLevel.sector) {
      sector = {
        id: segmentSpec.id,
        data: segmentSpec.yearlyData,
        cats: [],
        isIncluded: true,
      }

      sectors.push(sector)
    } else if (detailLevel === DetailLevel.cat) {
      cat = {
        id: segmentSpec.id,
        data: segmentSpec.yearlyData,
        subCats: [],
      }

      sector.cats.push(cat)
    } else if (detailLevel === DetailLevel.subCat) {
      subCat = { id: segmentSpec.id, data: segmentSpec.yearlyData }
      cat.subCats.push(subCat)
    } else {
      throw new Error('Invalid detail level: ' + detailLevel)
    }
  }

  return sectors
}

const parseMemo = (lines: string[]): Bunkers => {
  let data: YearlyData
  let aviation: YearlyData
  let navigation: YearlyData

  let line: string

  while (true) {
    line = lines.shift()

    if (!line.trim()) break

    const [identifier, ...strYearlyData] = line.split(fieldSeparator)
    const yearlyData = strYearlyData.map(parseAmount)

    if (identifier === MemoToken.bunkers) {
      data = yearlyData
    } else if (identifier === MemoToken.aviation) {
      aviation = yearlyData
    } else if (identifier === MemoToken.navigation) {
      navigation = yearlyData
    } else {
      throw new Error('Invalid memo token: ' + identifier)
    }
  }

  return { data, aviation, navigation }
}

const isTotalsToken = (strToken: string): strToken is TotalsToken => strToken in TotalsToken

const parseTotalsToken = (line: string): TokenizedLine<TotalsToken> => {
  const tokenizedLine = parseToken(line)

  if (!isTotalsToken(tokenizedLine.token)) {
    throw new Error('Unrecognized totals token: ' + tokenizedLine.token)
  }

  return tokenizedLine as TokenizedLine<TotalsToken>
}

const parseTotals = (lines: string[]): GasTotals => {
  let withoutLulucfText: string
  let withLulucfText: string
  let withoutLulucf: YearlyData
  let withLulucf: YearlyData

  let line: string

  while (line = lines.shift()) {
    const { token, rest } = parseTotalsToken(line)

    switch(token) {
      case TotalsToken.textWithout:
        withoutLulucfText = rest
        break
      case TotalsToken.textWith:
        withLulucfText = rest
        break
      case TotalsToken.without:
        const strWithoutYearlyData = rest.split(fieldSeparator)
        withoutLulucf = strWithoutYearlyData.map(parseAmount)
        break
      case TotalsToken.with:
        const strWithYearlyData = rest.split(fieldSeparator)
        withLulucf = strWithYearlyData.map(parseAmount)
        break
      default:
        throw new Error('Invalid totals token: ' + token)
    }
  }

  return { withoutLulucfText, withLulucfText, withoutLulucf, withLulucf }
}

const parseData = (strData: string): Gas => {
  const lines = strData.split(lineSeparator)
  const gas: Partial<Gas> = {}

  while (lines.length) {
    const line = lines.shift().trim()

    if (!line) continue

    const { token, rest } = parseDataToken(line)

    switch (token) {
      case DataToken.id:
        if (!isGasId(rest)) {
          throw new Error('Invalid gas id: ' + rest)
        }
        gas.id = rest
        break
      case DataToken.name:
        gas.name = rest
        break
      case DataToken.unit:
        gas.unit = rest
        break
      case DataToken.years:
        const { startYear, numYears } = parseYears(rest)
        gas.startYear = startYear
        gas.numYears = numYears
        break
      case DataToken.sectors:
        gas.sectors = parseSectors(lines)
        break
      case DataToken.memo:
        gas.bunkers = parseMemo(lines)
        break
      case DataToken.total:
        gas.totals = parseTotals(lines)
        break
      default:
        throw new Error('Invalid gas data token: ' + token)
    }
  }

  return gas as Gas
}

const loadGasData = async (
  periodType: PeriodType,
  gasId: GasId,
  measure?: Measure,
): Promise<Gas> => {
  let path: string

  if (periodType === PeriodType.past) {
    path = `data/past/${gasId}.ghg`
  } else if (periodType === PeriodType.prognosis) {
    path = `data/prognosis/${measure}/${gasId}.ghg`
  } else {
    assertNever(periodType, 'period type')
  }

  const response = await fetch(path)
  const strData = await response.text()
  return parseData(strData)
}

export const loadPastGasesData = async (): Promise<GasesData> => {
  const promises: Promise<Gas>[] = []

  for (let gasId in GasId) {
    promises.push(loadGasData(PeriodType.past, GasId[gasId]))
  }

  const arrGases = await Promise.all(promises)
  const gasesData: GasesData = new Map()
  arrGases.forEach((gas) => gasesData.set(gas.id, gas))
  return gasesData
}

const wemGasIds: GasId[] = [GasId.co2eq, GasId.co2, GasId.ch4, GasId.n2o, GasId.hfc, GasId.sf6]

export const loadPrognosisWemGasesData = async (): Promise<GasesData> => {
  const promises: Promise<Gas>[] = []

  for (let gasId of wemGasIds) {
    promises.push(loadGasData(PeriodType.prognosis, GasId[gasId], Measure.wem))
  }

  const arrGases = await Promise.all(promises)
  const gasesData: GasesData = new Map()
  arrGases.forEach((gas) => gasesData.set(gas.id, gas))
  return gasesData
}

const wamGasIds: GasId[] = [GasId.co2eq, GasId.co2, GasId.ch4, GasId.n2o]

export const loadPrognosisWamGasesData = async (): Promise<GasesData> => {
  const promises: Promise<Gas>[] = []

  for (let gasId of wamGasIds) {
    promises.push(loadGasData(PeriodType.prognosis, gasId, Measure.wam))
  }

  const arrGases = await Promise.all(promises)
  const gasesData: GasesData = new Map()
  arrGases.forEach((gas) => gasesData.set(gas.id, gas))
  return gasesData
}

const parseProportionSector = (line: string): Segment => {
  const [id, ...strYearlyData] = line.split(fieldSeparator)
  validateSegmentId(id, DetailLevel.sector)
  const yearlyData = strYearlyData.map(parseAmount)
  return { id, data: yearlyData }
}

const parseProportionSectors = (lines: string[]): ProportionData => {
  const sectors: { [key: string]: YearlyData } = {}

  while (lines.length) {
    const line = lines.shift().trim()
    const { id, data } = parseProportionSector(line)
    sectors[id] = data
  }

  return sectors
}

const parseProportionData = (strData: string): Proportion => {
  const lines = strData.split(lineSeparator)
  const proportion: Partial<Proportion> = {}

  while (lines.length) {
    const line = lines.shift().trim()

    if (!line) continue

    const { token, rest } = parseDataToken(line)

    switch (token) {
      case DataToken.years:
        const { startYear, numYears } = parseYears(rest)
        proportion.startYear = startYear
        proportion.numYears = numYears
        break
      case DataToken.sectors:
        proportion.sectors = parseProportionSectors(lines)
        break
      default:
        throw new Error('Invalid proportion data token: ' + token)
    }
  }

  return proportion as Proportion
}

// TODO: separation of concerns - data loading vs parsing
// TODO: past vs prognosis on this as well?
export const loadProportionData = async (): Promise<Proportion> => {
  const response = await fetch('data/past/proportion.ghgd')
  const strData = await response.text()
  return parseProportionData(strData)
}