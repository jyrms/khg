export enum PeriodType {
  past = 'past',
  prognosis = 'prognosis',
}

export enum GasId {
  co2eq = 'co2eq',
  co2 = 'co2',
  ch4 = 'ch4',
  n2o = 'n2o',
  hfc = 'hfc',
  pfc = 'pfc',
  sf6 = 'sf6',
}

export enum DetailLevel {
  total = 'total',
  sector = 'sector',
  cat = 'cat',
  subCat = 'subCat',
}

export enum Measure {
  wem = 'wem',
  wam = 'wam',
}

export type YearlyData = number[]

export interface SectorSpec {
  id: string,
  name: string,
  idxColor: number,
}

export interface CatSpec extends SectorSpec {
  sectorId: string,
}

export interface SubCatSpec extends SectorSpec {
  catId: string,
}

export interface LegendElement {
  name: string,
  idxColor: number,
}

export interface FilterableSegment {
  id: string,
  isIncluded: boolean,
}

export interface Segment {
  id: string,
  data: YearlyData,
}

export interface SubCat extends Segment {}

export interface Cat extends Segment {
  subCats: SubCat[],
}

export interface Sector extends Segment {
  cats: Cat[],
  isIncluded: boolean,
}

export interface GasTotals {
  withoutLulucfText: string,
  withLulucfText: string,
  withoutLulucf: YearlyData,
  withLulucf: YearlyData,
}

export interface Bunkers {
  data: YearlyData,
  aviation: YearlyData,
  navigation: YearlyData
}

export interface Gas {
  id: GasId,
  name: string,
  unit: string,
  startYear: number,
  numYears: number,
  sectors: Sector[],
  bunkers?: Bunkers,
  totals?: GasTotals,
}

export type GasesData = Map<GasId, Gas>

export type ProportionData = { [key: string]: YearlyData }

export interface Proportion {
  startYear: number,
  numYears: number,
  sectors: ProportionData
}

export interface Year {
  idx: number,
  year: number,
  isIncluded: boolean,
}

export enum ChartTabId {
  general = 'general',
  pie = 'pie',
}

export interface TooltipProps {
  x: number,
  y: number,
  text: string,
}

export interface Row {
  name: string,
  data: YearlyData,
}

// TODO: dedup???
export interface SimpleData {
  // TODO: restrict using enum???
  id: string,
  name: string,
  startYear: number,
  numYears: number,
  rows: Row[],
  text: string,
}

export interface Bounds { min: number, max: number }

export interface Bar { top: number, bottom: number, left: number, right: number, amt: number }