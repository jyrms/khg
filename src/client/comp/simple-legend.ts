import { createElement as el, FC } from 'react'
import { LegendItem } from './legend-item'

interface Props { labels: string[] }

export const SimpleLegend: FC<Props> = ({ labels }) => el('div',
  { className: 'right-pane' },
  el('ul', null,
    labels.map((label, idx) => el(LegendItem, { key: idx, idxColor: idx }, label))
  ),
)