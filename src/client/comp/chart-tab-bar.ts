import { createElement as el, FC } from 'react'

import { ChartTabId } from '../types'
import { TabBar } from './tab-bar'

interface Props { activeTabId: ChartTabId, onSelect: (tabId: ChartTabId) => void }

export const ChartTabBar: FC<Props> = ({ activeTabId, onSelect }) => {
  const tabSpecs = [
    {
      id: ChartTabId.general,
      text: 'General',
      isActive: activeTabId === ChartTabId.general,
    },
    {
      id: ChartTabId.pie,
      text: 'Pie chart',
      isActive: activeTabId === ChartTabId.pie,
    },
  ]

  return el(TabBar, { tabSpecs, onSelect: (tabId) => onSelect(tabId as ChartTabId) })
}