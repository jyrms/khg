import { createElement as el, FC } from 'react'

import { ColorBox } from './color-box'

interface Props { idxColor: number }

export const LegendItem: FC<Props> = ({ idxColor, children }) => el('li', null,
  el(ColorBox, { idxColor }),
  children,
)