import { createElement as el, FC, ChangeEvent } from 'react'

interface Props {
  startYear: number,
  numYears: number,
  // This is idxYear not year
  value: number,
  onSelect: (idxYear: number) => void,
}

const renderOptions = (startYear: number, numYears: number) => {
  const options = []
  let year = startYear

  for (let idxYear = 0; idxYear < numYears; idxYear++) {
    options.push(el('option', { key: year, value: idxYear }, year))

    year++
  }

  return options
}

export const YearSelect: FC<Props> = ({ startYear, numYears, value, onSelect }: Props) => {
  return el('div', null,
    'Year: ',
    el('select',
      {
        value,
        onChange: (evt: ChangeEvent<HTMLSelectElement>) => onSelect(parseInt(evt.target.value)),
      },
      renderOptions(startYear, numYears),
    ),
  )
}