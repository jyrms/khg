import { createElement as el, FC, ChangeEvent } from 'react'

import { GasId, GasesData } from '../types'

interface Props { gasesData: GasesData, value: string, onSelect: (val: GasId) => void }

const renderOptions = (gasesData: GasesData) => {
  const options = []

  gasesData.forEach(
    (gas, gasId) => options.push(el('option', { key: gasId, value: gasId }, gas.name))
  )

  return options
}

export const GasSelect: FC<Props> = ({ gasesData, value, onSelect }: Props) => el('div', null,
  'Pollutant: ',
  el('select',
    {
      value: value,
      onChange: (evt: ChangeEvent<HTMLSelectElement>) => onSelect(evt.target.value as GasId)
    },
    renderOptions(gasesData),
  ),
)