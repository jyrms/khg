import { createElement as el, FC } from 'react'

import { cats, sectors, subCats } from '../segments'
import { DetailLevel, SectorSpec } from '../types'
import { LegendItem } from './legend-item'

interface Props { detailLevel: DetailLevel, ids: string[] }

const renderSegment = (detailLevel: DetailLevel, id: string) => {
  let segmentSpec: SectorSpec

  if (detailLevel === DetailLevel.sector) {
    segmentSpec = sectors.get(id)
  } else if (detailLevel === DetailLevel.cat) {
    segmentSpec = cats.get(id)
  } else if (detailLevel === DetailLevel.subCat) {
    segmentSpec = subCats.get(id)
  } else {
    throw new Error('Invalid level of detail: ' + detailLevel)
  }

  const { name, idxColor } = segmentSpec

  return el(LegendItem,
    { key: id, idxColor },
    name,
  )
}

export const SegmentLegend: FC<Props> = ({ detailLevel, ids }) => el('div',
  { id: 'main-right' },
  el('ul', null, ids.map((id) => renderSegment(detailLevel, id))),
)