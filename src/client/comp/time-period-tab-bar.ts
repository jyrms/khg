import { createElement as el, FC } from 'react'

import { PeriodType } from '../types'
import { TabBar } from './tab-bar'

interface Props { activeTabId: PeriodType, onSelect: (tabId: PeriodType) => void }

export const TimePeriodTabBar: FC<Props> = ({ activeTabId, onSelect }) => {
  const tabSpecs = [
    {
      id: PeriodType.past,
      text: 'Past data',
      isActive: activeTabId === PeriodType.past,
    },
    {
      id: PeriodType.prognosis,
      text: 'Projections',
      isActive: activeTabId === PeriodType.prognosis,
    },
  ]

  return el(TabBar, { tabSpecs, onSelect: (tabId) => onSelect(tabId as PeriodType) })
}