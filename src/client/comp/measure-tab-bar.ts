import { createElement as el, FC } from 'react'

import { Measure } from '../types'
import { TabBar } from './tab-bar'

interface Props { activeTabId: Measure, onSelect: (tabId: Measure) => void }

export const MeasureTabBar: FC<Props> = ({ activeTabId, onSelect }) => {
  const tabSpecs = [
    {
      id: Measure.wem,
      text: 'With existing measures',
      isActive: activeTabId === Measure.wem,
    },
    {
      id: Measure.wam,
      text: 'With additional measures',
      isActive: activeTabId === Measure.wam,
    },
  ]

  return el(TabBar, { tabSpecs, onSelect: (tabId) => onSelect(tabId as Measure) })
}