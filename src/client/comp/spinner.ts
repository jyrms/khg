import { createElement as el, FC } from 'react'

export const Spinner: FC = () => el('div',
  { className: 'text-center' },
  el('div',
    { className: 'spinner-border text-primary' },
    el('span', { className: 'sr-only' }, 'Loading...'),
  ),
)