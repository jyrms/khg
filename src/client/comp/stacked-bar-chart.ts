import {
  createElement as el,
  Component,
  MouseEvent as ReactMouseEvent,
} from 'react'

import { TooltipProps, SimpleData, Bounds, Bar } from '../types'
import { black, getConsecutiveColor } from '../colors'
import { formatGraphStepNumber, formatBarChartTooltipNumber } from '../number-format'

interface Props {
  data: SimpleData,
  setTooltip: (tip?: TooltipProps) => void,
}

const width = 800
const height = 600
const paddingTop = 10
const paddingBottom = 30
const paddingLeft = 60
const paddingRight = 10
const paddingText = 3
const minBarHeight = 5
const scaleLineWidth = 1

export class StackedBarChart extends Component<Props> {
  private ctx: CanvasRenderingContext2D

  private bounds: Bounds

  private amplitude: number

  private yBase: number

  private valStep?: number

  private bars?: Bar[]

  private highlitBar?: Bar

  componentDidMount() {
    const canvas  = this.refs.canvas as HTMLCanvasElement
    this.ctx = canvas.getContext('2d')
    this.renderGraph()
  }

  componentDidUpdate() {
    this.renderGraph()
  }

  private handleMouseMove(evt: ReactMouseEvent<HTMLElement, MouseEvent>) {
    const { canvas } = this.ctx
    const { top, left } = canvas.getBoundingClientRect()
    const x = evt.clientX - left
    const y = evt.clientY - top
    const bar = this.findBar(x, y)

    if (bar) {
      this.highlitBar = bar
      const formattedTotal = formatBarChartTooltipNumber(bar.amt)
      this.props.setTooltip({ x: evt.clientX, y: evt.clientY, text: formattedTotal })
    } else {
      this.highlitBar = undefined
      this.removeTooltip()
    }
  }

  private findBar(x: number, y: number): Bar | undefined {
    return this.bars.find((bar) => {
      return x >= bar.left && x <= bar.right && y >= bar.top && y <= bar.bottom
    })
  }

  private removeTooltip() {
    this.props.setTooltip()
  }

  private calculateBounds() {
    const { numYears, rows } = this.props.data

    const posSums: number[] = []
    const negSums: number[] = []

    for (let idx = 0; idx < numYears; idx++) {
      const posSum = rows.reduce((sum, { data }) => {
        const val = data[idx]
        return val > 0 ? sum + val : sum
      }, 0)

      posSums.push(posSum)

      const negSum = rows.reduce((sum, { data }) => {
        const val = data[idx]
        return val < 0 ? sum + val : sum
      }, 0)

      negSums.push(negSum)
    }

    this.bounds = {
      max: Math.max(...posSums),
      min: Math.min(...negSums),
    }
  }

  private calculateAmplitude() {
    const { min, max } = this.bounds
    const nominalAmplitude = max - min
    this.amplitude = 1.15 * nominalAmplitude
  }

  private calculateYBase() {
    this.yBase = height - paddingBottom - this.calculateBarHeight(this.bounds.min)
  }

  private calculateLineStep() {
    const { amplitude } = this
    const approximateMagnitude = Math.log10(amplitude)
    const magnitude = Math.floor(approximateMagnitude)
    const valStepCandidate = Math.pow(10, magnitude)

    if (2 * valStepCandidate < amplitude) {
      this.valStep = valStepCandidate
    } else {
      this.valStep = 0.25 * valStepCandidate
    }
  }

  private clearCanvas() {
    this.ctx.clearRect(0, 0, width, height)
  }

  private calculateBarHeight(val: number) {
    const maxHeight = height - (paddingTop + paddingBottom)
    const nominalHeight = (Math.abs(val) / this.amplitude) * maxHeight
    return Math.max(nominalHeight, minBarHeight)
  }

  private renderYear(year: number, x: number) {
    const { ctx } = this

    ctx.save()
    ctx.fillStyle = black
    ctx.textBaseline = 'middle'
    const y = height - 0.25 * paddingBottom
    ctx.translate(x, y)
    ctx.rotate(-0.5 * Math.PI)
    ctx.fillText(year.toString(), 0, 0)
    ctx.restore()
  }

  private renderHorizontalLine(y: number) {
    const { ctx } = this

    ctx.beginPath()
    ctx.moveTo(0.5 * paddingLeft, y)
    ctx.lineTo(width - paddingRight, y)
    ctx.stroke()
  }

  private renderHorizontalLineText(val: number, y: number) {
    const strVal = Math.abs(val) < 1 ? val.toPrecision(1) : formatGraphStepNumber(val)
    const textWidth = this.ctx.measureText(strVal).width
    this.ctx.fillText(strVal, paddingLeft - textWidth - paddingText, y - paddingText)
  }

  private renderHorizontalLines() {
    const { ctx, yBase, valStep, bounds } = this

    ctx.strokeStyle = black
    ctx.fillStyle = black
    ctx.lineWidth = scaleLineWidth

    let val = 0
    let y = yBase
    const yStep = this.calculateBarHeight(valStep)

    while (val <= bounds.max) {
      this.renderHorizontalLine(y)
      this.renderHorizontalLineText(val, y)

      val += valStep
      y -= yStep
    }

    val = -valStep
    y = this.yBase + yStep

    while (val >= bounds.min) {
      this.renderHorizontalLine(y)
      this.renderHorizontalLineText(val, y)

      val -= valStep
      y += yStep
    }
  }

  private highlightBar() {
    if (!this.highlitBar) {
      return
    }

    const { ctx } = this
    const { top, bottom, left, right } = this.highlitBar

    const width = right - left
    const height = bottom - top

    ctx.fillStyle = '#ffffff88'
    ctx.fillRect(left, top, width, height)
  }

  private renderBarChart() {
    const { ctx, props, yBase } = this
    const { startYear, numYears, rows } = props.data

    this.bars = []

    this.clearCanvas()

    const barWidth = (width - (paddingLeft + paddingRight)) / numYears
    let x = paddingLeft

    for (let idxYear = 0; idxYear < numYears; idxYear++) {
      let posY = yBase
      let negY = yBase

      const right = x + barWidth

      rows.forEach((row, idxRow) => {
        const val = row.data[idxYear]
        const barHeight = this.calculateBarHeight(val)
        ctx.fillStyle = getConsecutiveColor(idxRow)

        let barTop: number
        let barBottom: number

        if (val > 0) {
          barBottom = posY
          posY -= barHeight
          barTop = posY
          ctx.fillRect(x, posY, barWidth, barHeight)
        } else if (val < 0) {
          ctx.fillRect(x, negY, barWidth, barHeight)
          barTop = negY
          negY += barHeight
          barBottom = negY
        }

        const bar = { segment: row, top: barTop, bottom: barBottom, left: x, right, amt: val }
        this.bars.push(bar)
      })

      const yearX = (x + right) / 2
      this.renderYear(startYear + idxYear, yearX)

      x = right
    }
  }

  private renderGraph() {
    this.calculateBounds()
    this.calculateAmplitude()
    this.calculateYBase()
    this.calculateLineStep()

    this.renderBarChart()

    this.renderHorizontalLines()
    this.highlightBar()
  }

  render() {
    if (!this.props.data.rows.length) {
      return el('canvas',
        {
          ref: 'canvas',
          width,
          height,
          style: { display: 'none' },
        },
      )
    }

    return el('canvas',
      {
        ref: 'canvas',
        width,
        height,
        onMouseMove: (evt) => this.handleMouseMove(evt),
        onMouseLeave: () => this.removeTooltip(),
      },
    )
  }
}
