import { createElement as el, FC, Fragment, useEffect, useState } from 'react'
import { loadEnergyData } from '../data/energy'
import { SimpleData, TooltipProps } from '../types'
import { SimpleLegend } from './simple-legend'
import { Spinner } from './spinner'
import { StackedBarChart } from './stacked-bar-chart'
import { Tooltip } from './tooltip'

// TODO: factor out reusable SimpleChart?
export const EnergyChart: FC = () => {
  const [data, setData] = useState<SimpleData>()
  const [tooltip, setTooltip] = useState<TooltipProps>()

  useEffect(() => {
    const load = async () => {
      const energyData = await loadEnergyData()
      setData(energyData)
    }

    load()
  })

  if (!data) {
    return el(Spinner)
  }

  const renderTooltip = () => {
    if (!tooltip) return
    return el(Tooltip, tooltip)
  }

  const labels = data.rows.map(({ name }) => name)

  return el(Fragment, null,
    el('h2', { className: 'border-top' }, data.name),
    el('div',
      { className: 'simple-container' },
      el('div',
        { className: 'left-pane' },
        el(StackedBarChart, { data, setTooltip }),
      ),
      el(SimpleLegend, { labels }),
    ),
    el('div', null, data.text),
    renderTooltip(),
  )
}