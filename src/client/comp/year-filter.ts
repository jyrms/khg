import { createElement as el, Component, ChangeEvent } from 'react'

import { CaretRight } from './caret-right'
import { CaretDown } from './caret-down'
import { Year } from '../types'

interface Props {
  years: Year[]
  onChange: (idxYear: number) => void,
  includeAll: () => void,
  excludeAll: () => void,
}

interface State { isOpen: boolean }

export class YearFilter extends Component<Props, State> {

  state: State = { isOpen: false }

  private toggleOpen() {
    const isOpen = !this.state.isOpen
    this.setState({ isOpen })
  }

  private renderArrow() {
    return this.state.isOpen ? el(CaretDown) : el(CaretRight)
  }

  private isAllChecked() {
    return this.props.years.every((year) => year.isIncluded)
  }

  private toggleAll(evt: ChangeEvent<HTMLInputElement>) {
    if (evt.target.checked) {
      this.props.includeAll()
    } else {
      this.props.excludeAll()
    }
  }

  private renderAll() {
    const domId = 'chk-year-all'

    return el('div',
      {
        key: 'all',
        className: 'form-check'
      },
      el('input',
        {
          type: 'checkbox',
          id: domId,
          checked: this.isAllChecked(),
          onChange: (evt) => this.toggleAll(evt)
        },
      ),
      ' ',
      el('label',
        {
          className: 'form-check-label',
          htmlFor: domId,
        },
        'All',
      ),
    )
  }

  private renderYear(year: Year) {
    const { idx, isIncluded } = year
    const domId = 'chk-year-'  + idx

    return el('div',
      {
        key: idx,
        className: 'form-check'
      },
      el('input',
        {
          type: 'checkbox',
          id: domId,
          checked: isIncluded,
          onChange: () => this.props.onChange(idx),
        },
      ),
      ' ',
      el('label',
        {
          className: 'form-check-label',
          htmlFor: domId,
        },
        year.year,
      ),
    )
  }

  private renderCardBody() {
    if (!this.state.isOpen) {
      return null
    }

    return el('div',
      { className: 'card-body' },
      this.renderAll(),
      this.props.years.map((year) => this.renderYear(year)),
    )
  }

  render() {
    return el('div',
      { className: 'card mt-1 mb-1' },
      el('div',
        {
          className: 'card-header',
          style: { cursor: 'pointer' },
          onClick: () => this.toggleOpen(),
        },
        el('span',
          { style: { position: 'relative', top: '-2px' } },
          this.renderArrow(),
        ),
        ' Filter by year',
      ),
      this.renderCardBody(),
    )
  }
}
