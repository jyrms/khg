import { createElement as el, FC } from 'react'
import { TooltipProps } from '../types'

export const Tooltip: FC<TooltipProps> = ({ x, y, text}) => el('div',
  { id: 'tooltip', style: { left: x, top: y - 50 } },
  text,
)