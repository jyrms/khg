import { createElement as el, Component, ChangeEvent } from 'react'

import { CaretRight } from './caret-right'
import { CaretDown } from './caret-down'
import { sectors } from '../segments'
import { FilterableSegment } from '../types'

interface Props {
  sectors: FilterableSegment[],
  onChange: (idxSector: number) => void,
  includeAll: () => void,
  excludeAll: () =>  void,
}

interface State { isOpen: boolean }

export class SectorFilter extends Component<Props, State> {

  state: State = { isOpen: false }

  private toggleOpen() {
    const isOpen = !this.state.isOpen
    this.setState({ isOpen })
  }

  private renderArrow() {
    return this.state.isOpen ? el(CaretDown) : el(CaretRight)
  }

  private isAllChecked() {
    return this.props.sectors.every((sector) => sector.isIncluded)
  }

  private toggleAll(evt: ChangeEvent<HTMLInputElement>) {
    if (evt.target.checked) {
      this.props.includeAll()
    } else {
      this.props.excludeAll()
    }
  }

  private renderAll() {
    const domId = 'chk-sector-all'

    return el('div',
      {
        key: 'all',
        className: 'form-check'
      },
      el('input',
        {
          type: 'checkbox',
          id: domId,
          checked: this.isAllChecked(),
          onChange: (evt) => this.toggleAll(evt)
        },
      ),
      ' ',
      el('label',
        {
          className: 'form-check-label',
          htmlFor: domId,
        },
        'All',
      ),
    )
  }

  private renderSector(sector: FilterableSegment, idx: number) {
    const domId = 'chk-sector-'  + idx
    const sectorSpec = sectors.get(sector.id)

    return el('div',
      {
        key: idx,
        className: 'form-check'
      },
      el('input',
        {
          type: 'checkbox',
          id: domId,
          checked: sector.isIncluded,
          onChange: () => this.props.onChange(idx),
        },
      ),
      ' ',
      el('label',
        {
          className: 'form-check-label',
          htmlFor: domId,
        },
        sectorSpec.name,
      ),
    )
  }

  private renderCardBody() {
    if (!this.state.isOpen) {
      return null
    }

    return el('div',
      { className: 'card-body' },
      this.renderAll(),
      this.props.sectors.map((sector, idx) => this.renderSector(sector, idx)),
    )
  }

  render() {
    if (this.props.sectors.length <= 1) {
      return null
    }

    return el('div',
      { className: 'card mt-1 mb-1' },
      el('div',
        {
          className: 'card-header',
          style: { cursor: 'pointer' },
          onClick: () => this.toggleOpen(),
        },
        el('span',
          { style: { position: 'relative', top: '-2px' } },
          this.renderArrow(),
        ),
        ' Filter by sector',
      ),
      this.renderCardBody(),
    )
  }
}

