import {
  createElement as el,
  Component,
  MouseEvent as ReactMouseEvent,
} from 'react'

import { TooltipProps, Segment, Year, GasTotals, DetailLevel, Bounds, Bar } from '../types'
import { black, getConsecutiveColor } from '../colors'
import { idxWithoutLulucfColor, idxWithLulucfColor } from '../totals'
import { formatGraphStepNumber, formatBarChartTooltipNumber } from '../number-format'
import { getSegmentSpec } from '../segments'

interface Props {
  detailLevel: DetailLevel,
  segments?: Segment[],
  totals?: GasTotals,
  unit: string,
  years: Year[],
  setTooltip: (tip?: TooltipProps) => void,
}

const width = 800
const height = 600
const paddingTop = 10
const paddingBottom = 30
const paddingLeft = 60
const paddingRight = 10
const paddingText = 3
const minBarHeight = 5
const pointRadius = 3
const graphLineWidth = 3
const scaleLineWidth = 1

export class MainChart extends Component<Props> {
  private ctx: CanvasRenderingContext2D

  private bounds: Bounds

  private amplitude: number

  private yBase: number

  private valStep?: number

  private bars?: Bar[]

  private highlitBar?: Bar

  componentDidMount() {
    const canvas  = this.refs.canvas as HTMLCanvasElement
    this.ctx = canvas.getContext('2d')

    if (this.props.segments.length) {
      this.renderGraph()
    }
  }

  componentDidUpdate() {
    if (this.props.segments.length) {
      this.renderGraph()
    }
  }

  private handleMouseMove(evt: ReactMouseEvent<HTMLElement, MouseEvent>) {
    const { canvas } = this.ctx
    const { top, left } = canvas.getBoundingClientRect()
    const x = evt.clientX - left
    const y = evt.clientY - top
    const bar = this.findBar(x, y)

    if (bar) {
      this.highlitBar = bar
      const formattedTotal = formatBarChartTooltipNumber(bar.amt)
      this.props.setTooltip({ x: evt.clientX, y: evt.clientY, text: formattedTotal })
    } else {
      this.highlitBar = undefined
      this.removeTooltip()
    }
  }

  private findBar(x: number, y: number): Bar | undefined {
    return this.bars.find((bar) => {
      return x >= bar.left && x <= bar.right && y >= bar.top && y <= bar.bottom
    })
  }

  private removeTooltip() {
    this.props.setTooltip()
  }

  private calculateBounds() {
    const { years, segments } = this.props

    const posSums: number[] = []
    const negSums: number[] = []

    years.forEach(({ idx }) => {
      const posSum = segments.reduce((sum, { data }) => {
        const val = data[idx]
        return val > 0 ? sum + val : sum
      }, 0)

      posSums.push(posSum)

      const negSum = segments.reduce((sum, { data }) => {
        const val = data[idx]
        return val < 0 ? sum + val : sum
      }, 0)

      negSums.push(negSum)
    })

    this.bounds = {
      max: Math.max(...posSums),
      min: Math.min(...negSums),
    }
  }

  private calculateAmplitude() {
    const { min, max } = this.bounds
    const nominalAmplitude = max - min
    this.amplitude = 1.15 * nominalAmplitude
  }

  private calculateYBase() {
    this.yBase = height - paddingBottom - this.calculateBarHeight(this.bounds.min)
  }

  private calculateLineStep() {
    const { amplitude } = this
    const approximateMagnitude = Math.log10(amplitude)
    const magnitude = Math.floor(approximateMagnitude)
    const valStepCandidate = Math.pow(10, magnitude)

    if (2 * valStepCandidate < amplitude) {
      this.valStep = valStepCandidate
    } else {
      this.valStep = 0.25 * valStepCandidate
    }
  }

  private clearCanvas() {
    this.ctx.clearRect(0, 0, width, height)
  }

  private calculateBarHeight(val: number) {
    const maxHeight = height - (paddingTop + paddingBottom)
    const nominalHeight = (Math.abs(val) / this.amplitude) * maxHeight
    return Math.max(nominalHeight, minBarHeight)
  }

  private renderYear(year: number, x: number) {
    const { ctx } = this

    ctx.save()
    ctx.fillStyle = black
    ctx.textBaseline = 'middle'
    const y = height - 0.25 * paddingBottom
    ctx.translate(x, y)
    ctx.rotate(-0.5 * Math.PI)
    ctx.fillText(year.toString(), 0, 0)
    ctx.restore()
  }

  private renderHorizontalLine(y: number) {
    const { ctx } = this

    ctx.beginPath()
    ctx.moveTo(0.5 * paddingLeft, y)
    ctx.lineTo(width - paddingRight, y)
    ctx.stroke()
  }

  private renderHorizontalLineText(val: number, y: number) {
    const strVal = Math.abs(val) < 1 ? val.toPrecision(1) : formatGraphStepNumber(val)
    const textWidth = this.ctx.measureText(strVal).width
    this.ctx.fillText(strVal, paddingLeft - textWidth - paddingText, y - paddingText)
  }

  private renderHorizontalLines() {
    const { ctx, yBase, valStep, bounds } = this

    ctx.strokeStyle = black
    ctx.fillStyle = black
    ctx.lineWidth = scaleLineWidth

    let val = 0
    let y = yBase
    const yStep = this.calculateBarHeight(valStep)

    while (val <= bounds.max) {
      this.renderHorizontalLine(y)
      this.renderHorizontalLineText(val, y)

      val += valStep
      y -= yStep
    }

    val = -valStep
    y = this.yBase + yStep

    while (val >= bounds.min) {
      this.renderHorizontalLine(y)
      this.renderHorizontalLineText(val, y)

      val -= valStep
      y += yStep
    }
  }

  private renderVerticalAxisLabel() {
    const { ctx } = this

    ctx.fillStyle = black
    const x = 0.5 * paddingLeft - 10
    const y = paddingTop + 0.5 * (height - paddingBottom - paddingTop)

    ctx.save()
    ctx.translate(x, y)
    ctx.rotate(-0.5 * Math.PI)
    ctx.fillText(this.props.unit, 0, 0)
    ctx.restore()
  }

  private highlightBar() {
    if (!this.highlitBar) {
      return
    }

    const { ctx } = this
    const { top, bottom, left, right } = this.highlitBar

    const width = right - left
    const height = bottom - top

    ctx.fillStyle = '#ffffff88'
    ctx.fillRect(left, top, width, height)
  }

  private renderBarChart() {
    const { ctx, props: { detailLevel, years, segments }, yBase } = this

    this.bars = []

    this.clearCanvas()

    const barWidth = (width - (paddingLeft + paddingRight)) / years.length
    let x = paddingLeft

    years.forEach((year) => {
      let posY = yBase
      let negY = yBase

      const right = x + barWidth

      segments.forEach((segment) => {
        const val = segment.data[year.idx]
        const barHeight = this.calculateBarHeight(val)
        const { idxColor } = getSegmentSpec(detailLevel, segment.id)
        ctx.fillStyle = getConsecutiveColor(idxColor)

        let barTop: number
        let barBottom: number

        if (val > 0) {
          barBottom = posY
          posY -= barHeight
          barTop = posY
          ctx.fillRect(x, posY, barWidth, barHeight)
        } else if (val < 0) {
          ctx.fillRect(x, negY, barWidth, barHeight)
          barTop = negY
          negY += barHeight
          barBottom = negY
        }

        const bar = { segment, top: barTop, bottom: barBottom, left: x, right, amt: val }
        this.bars.push(bar)
      })

      const yearX = (x + right) / 2
      this.renderYear(year.year, yearX)

      x = right
    })
  }

  private calculatePointY(val) {
    const height = this.calculateBarHeight(val)
    return this.yBase - height
  }

  private drawPoint(x: number, y: number) {
    const { ctx } = this

    ctx.beginPath()
    ctx.arc(x, y, pointRadius, 0, 2 * Math.PI)
    ctx.fill()
    ctx.stroke()
  }

  private drawLine(x1: number, y1: number, x2: number, y2: number) {
    const { ctx } = this

    ctx.lineWidth = graphLineWidth
    ctx.beginPath()
    ctx.moveTo(x1, y1)
    ctx.lineTo(x2, y2)
    ctx.stroke()
  }

  private drawLineSegment(x: number, y: number, color: string, prevX?: number, prevY?: number) {
    const { ctx } = this

    ctx.strokeStyle = color
    ctx.fillStyle = color
    this.drawPoint(x, y)

    if (prevX && prevY) {
      this.drawLine(prevX, prevY, x, y)
    }
  }

  private renderLineChart() {
    const { props: { totals, years } } = this

    this.bars = []
    this.clearCanvas()

    if (!totals) {
      return
    }

    const withoutLulucfColor = getConsecutiveColor(idxWithoutLulucfColor)
    const withLulucfColor = getConsecutiveColor(idxWithLulucfColor)

    const barWidth = (width - (paddingLeft + paddingRight)) / years.length
    let x = paddingLeft + 0.5 * barWidth
    let prevX: number
    let prevWithLulucfY: number
    let prevWithoutLulucfY: number

    this.props.years.forEach((year) => {
      const withLulucfY = this.calculatePointY(totals.withLulucf[year.idx])
      this.drawLineSegment(x, withLulucfY, withLulucfColor, prevX, prevWithLulucfY)
      const withoutLulucfY = this.calculatePointY(totals.withoutLulucf[year.idx])
      this.drawLineSegment(x, withoutLulucfY, withoutLulucfColor, prevX, prevWithoutLulucfY)
      prevWithLulucfY = withLulucfY
      prevWithoutLulucfY = withoutLulucfY

      prevX = x
      this.renderYear(year.year, x)
      x += barWidth
    })
  }

  private renderGraph() {
    this.calculateBounds()
    this.calculateAmplitude()
    this.calculateYBase()
    this.calculateLineStep()

    if (this.props.totals) {
      this.renderLineChart()
    } else {
      this.renderBarChart()
    }

    this.renderHorizontalLines()
    this.renderVerticalAxisLabel()
    this.highlightBar()
  }

  render() {
    if (!this.props.segments.length) {
      return el('canvas',
        {
          ref: 'canvas',
          width,
          height,
          style: { display: 'none' },
        },
      )
    }

    return el('canvas',
      {
        ref: 'canvas',
        width,
        height,
        onMouseMove: (evt) => this.handleMouseMove(evt),
        onMouseLeave: () => this.removeTooltip(),
      },
    )
  }
}
