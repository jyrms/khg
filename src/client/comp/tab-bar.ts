import { createElement as el, FC } from 'react'

interface TabSpec {
  id: string,
  text: string,
  isActive?: boolean,
}

interface Props {
  tabSpecs: TabSpec[]
  onSelect: (tabId: string) => void
}

export const TabBar: FC<Props> = ({ tabSpecs, onSelect }) => {
  const renderTab = ({ id, text, isActive }: TabSpec) => {
    const className = isActive ? 'nav-link active' : 'nav-link'

    return el('li',
      { key: id, className: 'nav-item' },
      el('a',
        {
          className,
          href: '#',
          onClick: () => onSelect(id),
        },
        text,
      ),
    )
  }

  return el('div',
    { className: 'tabs-container' },
    el('ul',
      { className: 'nav nav-pills' },
      tabSpecs.map(renderTab),
    ),
  )
}