import { createElement as el, FC, ChangeEvent } from 'react'

import { DetailLevel } from '../types'

interface Props {
  value: DetailLevel,
  includeTotals: boolean,
  onSelect: (val: DetailLevel) => void,
}

const detailLevelNames: { [key in DetailLevel]: string } = {
  [DetailLevel.total]: 'Total emissions',
  [DetailLevel.sector]: 'By sector',
  [DetailLevel.cat]: 'By category',
  [DetailLevel.subCat]: 'By sub-category',
}

const renderOptions = (includeTotals: boolean) => {
  const options = []

  for (let detailLevel in DetailLevel) {
    if (!includeTotals && detailLevel === DetailLevel.total) {
      continue
    }

    options.push(
      el('option', { key: detailLevel, value: detailLevel }, detailLevelNames[detailLevel])
    )
  }

  return options
}

export const DetailLevelSelect: FC<Props> = ({ value, includeTotals, onSelect }: Props) => {
  return el('div', null,
    'Level of detail: ',
    el('select',
      {
        value,
        onChange: (evt: ChangeEvent<HTMLSelectElement>) => onSelect(
          evt.target.value as DetailLevel,
        ),
      },
      renderOptions(includeTotals),
    ),
  )
}