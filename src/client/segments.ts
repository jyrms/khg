import { CatSpec, DetailLevel, SectorSpec, SubCatSpec } from './types'

const arrSectors: SectorSpec[] = [
  {
    id: 'energy',
    name: 'Energy',
    idxColor: 0,
  },
  {
    id: 'ind-proc',
    name: 'Industrial processes and product use',
    idxColor: 1,
  },
  {
    id: 'agri',
    name: 'Agriculture',
    idxColor: 2,
  },
  {
    id: 'lulucf',
    name: 'Land use, land-use change and forestry',
    idxColor: 3,
  },
  {
    id: 'waste',
    name: 'Waste',
    idxColor: 4,
  },
]

export const sectors: Map<string, SectorSpec> = new Map()
arrSectors.forEach((spec) => sectors.set(spec.id, spec))

const arrCats: CatSpec[] = [
  {
    id: 'energy-ind',
    name: 'Energy industries',
    idxColor: 0,
    sectorId: 'energy'
  },
  {
    id: 'manuf',
    name: 'Manufacturing industries and construction',
    idxColor: 1,
    sectorId: 'energy'
  },
  {
    id: 'transport',
    name: 'Transport',
    idxColor: 2,
    sectorId: 'energy'
  },
  {
    id: 'other-sec',
    name: 'Other sectors',
    idxColor: 3,
    sectorId: 'energy'
  },
  {
    id: 'other',
    name: 'Other',
    idxColor: 4,
    sectorId: 'energy'
  },
  {
    id: 'oil',
    name: 'Oil and natural gas and other emissions from energy production',
    idxColor: 5,
    sectorId: 'energy'
  },
  {
    id: 'mineral-ind',
    name: 'Mineral industry',
    idxColor: 6,
    sectorId: 'ind-proc'
  },
  {
    id: 'chem-ind',
    name: 'Chemical industry (Ammonia production)',
    idxColor: 7,
    sectorId: 'ind-proc'
  },
  {
    id: 'metal-ind',
    name: 'Metal industry (Lead production)',
    idxColor: 8,
    sectorId: 'ind-proc'
  },
  {
    id: 'non-energy',
    name: 'Non-energy products from fuels and solvent use',
    idxColor: 9,
    sectorId: 'ind-proc'
  },
  {
    id: 'ods',
    name: 'Product uses as ODS substitutes',
    idxColor: 10,
    sectorId: 'ind-proc'
  },
  {
    id: 'other-prod',
    name: 'Other product manufacture and use',
    idxColor: 11,
    sectorId: 'ind-proc'
  },
  {
    id: 'enteric',
    name: 'Enteric fermentation',
    idxColor: 12,
    sectorId: 'agri'
  },
  {
    id: 'manure',
    name: 'Manure management',
    idxColor: 13,
    sectorId: 'agri'
  },
  {
    id: 'agri-soils',
    name: 'Agricultural soils',
    idxColor: 14,
    sectorId: 'agri'
  },
  {
    id: 'liming',
    name: 'Liming',
    idxColor: 15,
    sectorId: 'agri'
  },
  {
    id: 'urea',
    name: 'Urea application',
    idxColor: 16,
    sectorId: 'agri'
  },
  {
    id: 'forest',
    name: 'Forest land',
    idxColor: 17,
    sectorId: 'lulucf'
  },
  {
    id: 'cropland',
    name: 'Cropland',
    idxColor: 18,
    sectorId: 'lulucf'
  },
  {
    id: 'grassland',
    name: 'Grassland',
    idxColor: 19,
    sectorId: 'lulucf'
  },
  {
    id: 'wetlands',
    name: 'Wetlands',
    idxColor: 20,
    sectorId: 'lulucf'
  },
  {
    id: 'settlements',
    name: 'Settlements',
    idxColor: 21,
    sectorId: 'lulucf'
  },
  {
    id: 'other-land',
    name: 'Other land',
    idxColor: 22,
    sectorId: 'lulucf'
  },
  {
    id: 'harv-wood',
    name: 'Harvested wood products',
    idxColor: 23,
    sectorId: 'lulucf'
  },
  {
    id: 'other-lulucf',
    name: 'Other',
    idxColor: 24,
    sectorId: 'lulucf',
  },
  {
    id: 'disposal',
    name: 'Solid waste disposal',
    idxColor: 25,
    sectorId: 'waste'
  },
  {
    id: 'bio-treat',
    name: 'Biological treatment of solid waste',
    idxColor: 26,
    sectorId: 'waste'
  },
  {
    id: 'incineration',
    name: 'Incineration and open burning of waste',
    idxColor: 27,
    sectorId: 'waste'
  },
  {
    id: 'waste-water',
    name: 'Waste water treatment and discharge',
    idxColor: 28,
    sectorId: 'waste'
  },
]

export const cats: Map<string, CatSpec> = new Map()
arrCats.forEach((spec) => cats.set(spec.id, spec))

const arrSubCats: SubCatSpec[] = [
  {
    id: 'pub-electric',
    name: 'Public electricity production',
    idxColor: 0,
    catId: 'energy-ind'
  },
  {
    id: 'pub-heat',
    name: 'Public heat production',
    idxColor: 1,
    catId: 'energy-ind'
  },
  {
    id: 'shale',
    name: 'Shale oil production',
    idxColor: 2,
    catId: 'energy-ind'
  },
  {
    id: 'iron-steel',
    name: 'Iron and steel',
    idxColor: 3,
    catId: 'manuf'
  },
  {
    id: 'non-ferr',
    name: 'Non-ferrous metals',
    idxColor: 4,
    catId: 'manuf'
  },
  {
    id: 'chemicals',
    name: 'Chemicals',
    idxColor: 5,
    catId: 'manuf'
  },
  {
    id: 'pulp',
    name: 'Pulp, paper and print',
    idxColor: 6,
    catId: 'manuf'
  },
  {
    id: 'food',
    name: 'Food processing, beverages and tobacco',
    idxColor: 7,
    catId: 'manuf'
  },
  {
    id: 'non-met',
    name: 'Non-metallic minerals',
    idxColor: 8,
    catId: 'manuf'
  },
  {
    id: 'other-manuf',
    name: 'Other',
    idxColor: 9,
    catId: 'manuf'
  },
  {
    id: 'domestic-avi',
    name: 'Domestic aviation',
    idxColor: 10,
    catId: 'transport'
  },
  {
    id: 'road',
    name: 'Road transportation',
    idxColor: 11,
    catId: 'transport'
  },
  {
    id: 'rail',
    name: 'Railways',
    idxColor: 12,
    catId: 'transport'
  },
  {
    id: 'domestic-navi',
    name: 'Domestic navigation',
    idxColor: 13,
    catId: 'transport'
  },
  {
    id: 'com',
    name: 'Commercial/institutional',
    idxColor: 14,
    catId: 'other-sec'
  },
  {
    id: 'resident',
    name: 'Residential',
    idxColor: 15,
    catId: 'other-sec'
  },
  {
    id: 'agri-forest',
    name: 'Agriculture/forestry/fishing',
    idxColor: 16,
    catId: 'other-sec'
  },
  {
    id: 'cement',
    name: 'Cement production',
    idxColor: 17,
    catId: 'mineral-ind'
  },
  {
    id: 'lime',
    name: 'Lime production',
    idxColor: 18,
    catId: 'mineral-ind'
  },
  {
    id: 'glass',
    name: 'Glass production',
    idxColor: 19,
    catId: 'mineral-ind'
  },
  {
    id: 'other-carb',
    name: 'Other process uses of carbonates',
    idxColor: 20,
    catId: 'mineral-ind'
  },
  {
    id: 'lubricant',
    name: 'Lubricant use',
    idxColor: 21,
    catId: 'non-energy'
  },
  {
    id: 'paraffin',
    name: 'Paraffin wax use',
    idxColor: 22,
    catId: 'non-energy'
  },
  {
    id: 'solvent',
    name: 'Solvent use',
    idxColor: 23,
    catId: 'non-energy'
  },
  {
    id: 'paving',
    name: 'Road paving with asphalt',
    idxColor: 24,
    catId: 'non-energy'
  },
  {
    id: 'urea-catalysts',
    name: 'Urea based catalysts for motor vehicle',
    idxColor: 25,
    catId: 'non-energy'
  },
  {
    id: 'refrigeration',
    name: 'Refrigeration and air conditioning',
    idxColor: 26,
    catId: 'ods'
  },
  {
    id: 'foam',
    name: 'Foam blowing agents',
    idxColor: 27,
    catId: 'ods'
  },
  {
    id: 'fire-protection',
    name: 'Fire protection',
    idxColor: 28,
    catId: 'ods'
  },
  {
    id: 'aerosols',
    name: 'Aerosols',
    idxColor: 29,
    catId: 'ods'
  },
  {
    id: 'adia',
    name: 'Adiabatic Properties: Shoes and Tyres',
    idxColor: 30,
    catId: 'ods'
  },
  {
    id: 'med-app',
    name: 'N₂O from product uses from medical applications',
    idxColor: 31,
    catId: 'other-prod'
  },
  {
    id: 'other-prod-use',
    name: 'SF₆ and PFCs from other product use',
    idxColor: 32,
    catId: 'other-prod'
  },
  {
    id: 'electrical-equip',
    name: 'Electrical Equipment',
    idxColor: 33,
    catId: 'other-prod'
  },
  {
    id: 'other-prod-sf6',
    name: 'SF₆ from other product use',
    idxColor: 34,
    catId: 'other-prod'
  },
]

export const subCats: Map<string, SubCatSpec> = new Map()
arrSubCats.forEach((spec) => subCats.set(spec.id, spec))

export const getSegmentSpec = (detailLevel: DetailLevel, id: string): SectorSpec => {
  // TODO: throw error if no segment spec with this id exists?
  if (detailLevel === DetailLevel.sector) {
    return sectors.get(id)
  } else if (detailLevel === DetailLevel.cat) {
    return cats.get(id)
  } else if (detailLevel === DetailLevel.subCat) {
    return subCats.get(id)
  } else {
    throw new Error('Invalid detail level: ' + detailLevel)
  }
}