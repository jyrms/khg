import { cloneDeep } from 'lodash'
import { createElement as el, Component } from 'react'

import { MainChart } from './comp/main-chart'
import { GasSelect } from './comp/gas-select'
import { SegmentLegend } from './comp/segment-legend'
import { Spinner } from './comp/spinner'

import {
  loadPastGasesData,
  loadPrognosisWamGasesData,
  loadPrognosisWemGasesData,
  loadProportionData,
} from './data'

import {
  GasesData,
  GasId,
  TooltipProps,
  DetailLevel,
  Sector,
  Cat,
  SubCat,
  Segment,
  Gas,
  Year,
  Proportion,
  ChartTabId,
  PeriodType,
  Measure,
} from './types'

import { DetailLevelSelect } from './comp/detail-level-select'
import { SectorFilter } from './comp/sector-filter'
import { YearFilter } from './comp/year-filter'
import { TotalsLegend } from './comp/totals-legend'
import { PieChart } from './comp/pie-chart'
import { ChartTabBar } from './comp/chart-tab-bar'
import { TimePeriodTabBar } from './comp/time-period-tab-bar'
import { assertNever } from './assert-never'
import { MeasureTabBar } from './comp/measure-tab-bar'
import { EnergyChart } from './comp/energy-chart'
import { Tooltip } from './comp/tooltip'

interface State {
  pastGasesData?: GasesData,
  prognosisWemGasesData?: GasesData,
  prognosisWamGasesData?: GasesData,
  periodType: PeriodType,
  chartTabId: ChartTabId,
  measure?: Measure,
  selectedGasId: GasId,
  detailLevel: DetailLevel,
  years?: Year[],
  proportion?: Proportion,
  tooltip?: TooltipProps,
}

export class Main extends Component<{}, State> {
  state: State = {
    periodType: PeriodType.past,
    chartTabId: ChartTabId.general,
    measure: Measure.wem,
    detailLevel: DetailLevel.total,
    selectedGasId: GasId.co2eq,
  }

  async componentDidMount() {
    const pastGasesPromise = loadPastGasesData()
    const prognosisWemGasesPromise = loadPrognosisWemGasesData()
    const prognosisWamGasesPromise = loadPrognosisWamGasesData()
    const proportionPromise = loadProportionData()

    const [
      pastGasesData,
      prognosisWemGasesData,
      prognosisWamGasesData,
      proportion
    ] = await Promise.all(
      [pastGasesPromise, prognosisWemGasesPromise, prognosisWamGasesPromise, proportionPromise],
    )

    const newState = { pastGasesData, prognosisWemGasesData, prognosisWamGasesData, proportion }

    this.setState(newState, () => {
      const years = this.getYears()
      this.setState({ years })
    })
  }

  private getYears(): Year[] {
    const years: Year[] = []
    const gas = this.getSelectedGas()
    let yearNumber = gas.startYear

    for (let idx = 0; idx < gas.numYears; idx++) {
      const year: Year = { idx, year: yearNumber, isIncluded: true }
      years.push(year)
      yearNumber++
    }

    return years
  }

  private getGas(gasId: GasId): Gas {
    const {
      periodType,
      measure,
      pastGasesData,
      prognosisWemGasesData,
      prognosisWamGasesData,
    } = this.state

    if (periodType === PeriodType.past) {
      return pastGasesData.get(gasId)
    } else if (periodType === PeriodType.prognosis) {
      if (measure === Measure.wem) {
        return prognosisWemGasesData.get(gasId)
      } else if (measure === Measure.wam) {
        return prognosisWamGasesData.get(gasId)
      } else {
        assertNever(measure, 'measure')
      }
    } else {
      assertNever(periodType, 'period type')
    }
  }

  private getSelectedGas(): Gas {
    return this.getGas(this.state.selectedGasId)
  }

  private extractSegments(): Segment[] {
    const { detailLevel } = this.state

    const gas = this.getSelectedGas()
    const includedSectors = gas.sectors.filter((sector) => sector.isIncluded)

    if (detailLevel === DetailLevel.sector || detailLevel === DetailLevel.total) {
      return includedSectors
    } else if (detailLevel === DetailLevel.cat) {
      let cats: Cat[] = []
      includedSectors.forEach((sector) => cats = cats.concat(sector.cats))
      return cats
    } else if (detailLevel === DetailLevel.subCat) {
      let subCats: SubCat[] = []

      includedSectors.forEach((sector) => {
        sector.cats.forEach((cat) => {
          subCats = subCats.concat(cat.subCats)
        })
      })

      return subCats
    } else {
      assertNever(detailLevel, 'detail level')
    }
  }

  private setPeriodType(periodType: PeriodType) {
    const nextState = {
      periodType,
      measure: Measure.wem,
      selectedGasId: GasId.co2eq,
      chartTabId: ChartTabId.general,
    }

    this.setState(nextState, () => {
      const years = this.getYears()
      this.setState({ years })
    })
  }

  private setMeasure(measure: Measure) {
    this.setState({ measure, selectedGasId: GasId.co2eq })
  }

  private setDetailLevel(detailLevel: DetailLevel) {
    this.setState({ detailLevel })
  }

  private setGas(gasId: GasId) {
    const { detailLevel } = this.state
    const years = this.getYears()

    const gas = this.getGas(gasId)

    const newDetailLevel = detailLevel === DetailLevel.total && !gas.totals ?
      DetailLevel.sector : detailLevel

    this.setState({ selectedGasId: gasId, years, detailLevel: newDetailLevel })
  }

  private getGasesDataVarName(): 'pastGasesData' | 'prognosisWemGasesData' | 'prognosisWamGasesData' {
    const { periodType, measure } = this.state

    if (periodType === PeriodType.past) {
      return 'pastGasesData'
    } else if (periodType === PeriodType.prognosis) {
      if (measure === Measure.wem) {
        return 'prognosisWemGasesData'
      } else if (measure === Measure.wam) {
        return 'prognosisWamGasesData'
      } else {
        assertNever(measure, measure)
      }
    } else {
      assertNever(periodType, 'period type')
    }
  }

  private getGasesData(): GasesData {
    const varName = this.getGasesDataVarName()
    return this.state[varName]
  }

  private toggleSectorIncluded(idxSector: number) {
    const { selectedGasId } = this.state
    const gasesDataVarName = this.getGasesDataVarName()

    const newGasesData = cloneDeep(this.state[gasesDataVarName])
    const sector = newGasesData.get(selectedGasId).sectors[idxSector]

    if (!sector) {
      throw new Error('Error toggling sector: invalid sector index: ' + idxSector)
    }

    sector.isIncluded = !sector.isIncluded
    const newState = { [gasesDataVarName]: newGasesData }
    // TODO: any
    this.setState(newState as any)
  }

  private includeAllSectors() {
    const { selectedGasId } = this.state
    const gasesDataVarName = this.getGasesDataVarName()
    const newGasesData = cloneDeep(this.state[gasesDataVarName])
    newGasesData.get(selectedGasId).sectors.forEach((sector) => sector.isIncluded = true)
    const newState = { [gasesDataVarName]: newGasesData }
    // TODO: any
    this.setState(newState as any)
  }

  private excludeAllSectors() {
    const { selectedGasId } = this.state
    const gasesDataVarName = this.getGasesDataVarName()
    const newGasesData = cloneDeep(this.state[gasesDataVarName])
    newGasesData.get(selectedGasId).sectors.forEach((sector) => sector.isIncluded = false)
    const newState = { [gasesDataVarName]: newGasesData }
    // TODO: any
    this.setState(newState as any)
  }

  private renderSectorFilter(sectors: Sector[]) {
    if (this.state.detailLevel === DetailLevel.total) {
      return
    }

    return el(SectorFilter,
      {
        sectors,
        onChange: (idx) => this.toggleSectorIncluded(idx),
        includeAll: () => this.includeAllSectors(),
        excludeAll: () => this.excludeAllSectors(),
      },
    )
  }

  private toggleYearIncluded(idxYear: number) {
    const years = cloneDeep(this.state.years)
    const year = years[idxYear]

    if (!year) {
      throw new Error('Error toggling year: invalid year index: ' + idxYear)
    }

    year.isIncluded = !year.isIncluded
    this.setState({ years })
  }

  private includeAllYears() {
    const years = cloneDeep(this.state.years)
    years.forEach((year) => year.isIncluded = true)
    this.setState({ years })
  }

  private excludeAllYears() {
    const years = cloneDeep(this.state.years)
    years.forEach((year) => year.isIncluded = false)
    this.setState({ years })
  }

  public setChartTabId(chartTabId: ChartTabId) {
    this.setState({ chartTabId })
  }

  public setTooltip(tooltip?: TooltipProps) {
    this.setState({ tooltip })
  }

  private renderMeasureTabBar() {
    if (this.state.periodType !== PeriodType.prognosis) {
      return null
    }

    return el(MeasureTabBar,
      {
        activeTabId: this.state.measure,
        onSelect: (measure) => this.setMeasure(measure),
      },
    )
  }

  private renderChartTabBar() {
    if (this.state.periodType === PeriodType.prognosis) {
      return null
    }

    return el(ChartTabBar,
      { activeTabId: this.state.chartTabId, onSelect: (tabId) => this.setChartTabId(tabId) },
    )
  }

  private renderGeneralChart() {
    const {
      periodType,
      pastGasesData,
      prognosisWemGasesData: prognosisGasesData,
      detailLevel,
      selectedGasId,
      years,
    } = this.state

    if (
      (periodType === PeriodType.past && !pastGasesData) ||
      (periodType === PeriodType.prognosis && !prognosisGasesData) ||
      !years
    ) {
      return el(Spinner)
    }

    const gasesData = this.getGasesData()
    const gas = this.getSelectedGas()
    const { unit, sectors, totals } = gas
    const includeTotals = Boolean(totals)
    const segments = this.extractSegments()
    const chartTotals = detailLevel === DetailLevel.total ? totals : undefined
    const includedYears = years.filter((year) => year.isIncluded)

    return el('div',
      { id: 'main-left' },
      el(GasSelect,
        {
          gasesData,
          value: selectedGasId,
          onSelect: (gasId) => this.setGas(gasId),
        },
      ),
      el(DetailLevelSelect,
        {
          value: detailLevel,
          includeTotals,
          onSelect: (detLev) => this.setDetailLevel(detLev),
        },
      ),
      this.renderSectorFilter(sectors),
      el(YearFilter,
        {
          years: this.state.years,
          onChange: (idx) => this.toggleYearIncluded(idx),
          includeAll: () => this.includeAllYears(),
          excludeAll: () => this.excludeAllYears(),
        },
      ),
      el(MainChart,
        {
          detailLevel,
          segments,
          totals: chartTotals,
          unit,
          years: includedYears,
          setTooltip: (tip) => this.setTooltip(tip),
        },
      ),
    )
  }

  private renderPieChart() {
    if (!this.state.proportion) {
      return el(Spinner)
    }

    return el('div',
      { id: 'main-left' },
      el(PieChart,
        {
          proportion: this.state.proportion,
          setTooltip: (tip) => this.setTooltip(tip),
        },
      ),
    )
  }

  private renderChart() {
    const { chartTabId } = this.state

    if (chartTabId === ChartTabId.general) {
      return this.renderGeneralChart()
    }

    if (chartTabId === ChartTabId.pie) {
      return this.renderPieChart()
    }

    assertNever(chartTabId, 'chart tab id')
  }

  private renderGeneralLegend() {
    const { periodType, pastGasesData, prognosisWemGasesData: prognosisGasesData, detailLevel } = this.state

    if (
      (periodType === PeriodType.past && !pastGasesData) ||
      (periodType === PeriodType.prognosis && !prognosisGasesData)
    ) {
      // TODO: we show two spinners: one for chart, one for legend. Unnecessary
      return el(Spinner)
    }

    const gas = this.getSelectedGas()

    if (detailLevel === DetailLevel.total) {
      const { withoutLulucfText, withLulucfText } = gas.totals
      return el(TotalsLegend, { withoutLulucfText, withLulucfText })
    }

    const segments = this.extractSegments()
    const ids = segments.map(({ id }) => id)
    return el(SegmentLegend, { detailLevel, ids })
  }

  private renderPieChartLegend() {
    const { proportion } = this.state

    if (!proportion) {
      return el(Spinner)
    }

    return el(SegmentLegend,
      {
        detailLevel: DetailLevel.sector,
        ids: Object.keys(proportion.sectors),
      },
    )
  }

  private renderLegend() {
    const { chartTabId } = this.state

    if (chartTabId === ChartTabId.general) {
      return this.renderGeneralLegend()
    }

    if (chartTabId === ChartTabId.pie) {
      return this.renderPieChartLegend()
    }

    assertNever(chartTabId, 'chart tab id')
  }

  private renderTooltip() {
    const { tooltip } = this.state

    if (!tooltip || !tooltip.text) return

    return el(Tooltip, tooltip)
  }

  private renderSecondaryCharts() {
    if (this.state.periodType === PeriodType.prognosis) return
    return el(EnergyChart)
  }

  render() {
    const { periodType, chartTabId } = this.state

    return el('div',
      { id: 'main' },
      el(TimePeriodTabBar,
        { activeTabId: periodType, onSelect: (tabId) => this.setPeriodType(tabId) },
      ),
      this.renderMeasureTabBar(),
      this.renderChartTabBar(),
      el('div',
        { id: 'main-container' },
        this.renderChart(),
        this.renderLegend(),
      ),
      this.renderTooltip(),
      this.renderSecondaryCharts(),
    )
  }
}