export const assertNever = (value: never, description: string, valueForMessage?: string) => {
  throw new Error('Unexpected ' + description + ': ' + (valueForMessage || value))
}