import * as express from 'express'

const app = express()

app.use(express.static('public'))

// TODO: different port on production
const port = 3000

app.listen(port, () => console.log('Server listening on port ' + port))
