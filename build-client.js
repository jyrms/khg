const fs = require('fs')

const browserify = require('browserify')
const tsify = require('tsify')
const watchify = require('watchify')

const b = browserify({
  entries: 'src/client/index.ts',
  cache: {},
  packageCache: {},
  plugin: [watchify]
})
.plugin(tsify, { target: 'ESNext'})

const bundle = () => {
  console.log('building client')
  b.bundle()
  .on('error', (err) => { console.error(err.toString()) })
  .pipe(fs.createWriteStream('public/js/bundle.js'))
}

b.on('update', bundle)
bundle()
